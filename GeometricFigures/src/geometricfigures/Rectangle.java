/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometricfigures;

/**
 *
 * @author Andra-Maria
 */
public class Rectangle extends Shape{
     private final double width;
     private final double length;
 
    public Rectangle(double width, double length){
        this.width = width;
        this.length = length;
    }
    
     //Area of a rectangle is: length * width
     @Override
    public double area(){
        
        return length * width;
       
    }
    
    //Perimeter of a rectangle is: 2*(length+width)
     @Override
    public double perimeter(){
        
        return 2 * ( length + width );
        
    }
    
}
