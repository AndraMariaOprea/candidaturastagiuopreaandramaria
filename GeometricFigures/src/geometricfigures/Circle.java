/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometricfigures;

/**
 *
 * @author Andra-Maria
 */
public class Circle extends Shape{
    
    private static final double PI = Math.PI;
    private final double radius;

    public  Circle(double radius){
        this.radius = radius;
    }
    
    //Area of a circle is: pi*r^2
    @Override
    public double area(){
        
        
        return PI * Math.pow(radius, 2);
       
    }
    
    //Perimeter of a circle is: 2*pi*r
    @Override
    public double perimeter(){
        
        return 2 * PI * radius;
        
    }
    
    
}
