/*
1. Given a choice of geometric figures, calculate the chosen figure’s area and perimeter. 
The required lengths will also be input by the user.
The user can choose between a circle, a triangle or a rectangle. 
User input should be taken via console.
*/
package geometricfigures;

import java.util.Scanner;
/**
 *
 * @author Andra-Maria
 */
public class GeometricFigures {

    
    public static void main(String[] args) {

        int menuOption;

        menuOption = showMenu();
        processShapeOption(menuOption);
        
    } //end main
    
     public static int showMenu() {
       Scanner scan = new Scanner(System.in);
       int option;

       System.out.println("1. circle");
       System.out.println("2. triangle");
       System.out.println("3. rectangle");
       System.out.print("enter your choice:");
       
       option = scan.nextInt();

       return option;
   }
     
    public static double getDimension(String dimensionType) {
        double dimension;
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter the " + dimensionType + ":");
        dimension = scan.nextDouble();

        return dimension;
    }
     
     public static void processShapeOption(int option) {

        switch (option) {
            case 1:
                double radius = getDimension("radius");
                Shape circle = new Circle(radius);
                System.out.println("Circle Area: " + circle.area()
                        + "\nCircle Perimeter: " + circle.perimeter() + "\n");
                break;

            case 2:
                double a = getDimension("a"),
                 b = getDimension("b"),
                 c = getDimension("c");
                Shape triangle = new Triangle(a, b, c);
                System.out.println("Triangle Area: " + triangle.area()
                        + "\nTriangle Perimeter: " + triangle.perimeter() + "\n");
                break;

            case 3:
                double length = getDimension("length"),
                 width = getDimension("width");
                Shape rectangle = new Rectangle(width, length);
                System.out.println("\nRectangle Area: " + rectangle.area()
                        + "\nRectangle Perimeter: " + rectangle.perimeter() + "\n");
                break;

        }
    }
     
}


