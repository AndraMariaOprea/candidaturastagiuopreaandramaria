/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometricfigures;

/**
 *
 * @author Andra-Maria
 */
public class Triangle extends Shape{

    private final double a;
    private final double b;
    private final double c;
    
    public Triangle(double a, double b, double  c){
        this.a = a;
        this.b = b;
        this.c = c;
    }

    //Area of a triangle is: Heron`s formula: sqrt(s*(s-a)*(s-b)*(s-c)), s-semiperimeter
    @Override
    public double area(){
        
        double s = (a+b+c)/2;
        return Math.sqrt(s*(s-a)*(s-b)*(s-c));
       
    }
    
    //Perimeter of a triangle is: a+b+c
    @Override
    public double perimeter(){
        
        return a+b+c;
        
    }
}
