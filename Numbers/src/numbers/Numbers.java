/*
 * 2. Given an input list of numbers, find the largest and the smallest numbers, print any numbers that are palindromes, 
 * and any numbers that are prime numbers.
Example: Given a list of [234, 678, 131, 101, 199, 346], the output should be:
Largest number: 678
Smallest number: 101
Palindromes: 131, 101
Prime numbers: 101, 131, 199
 */
package numbers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author Andra-Maria
 *
 */
public class Numbers {
    
    public static void main(String[] args) {

        ArrayList<Integer> numbers = new ArrayList<Integer>();
        ArrayList<Integer> palindrome = new ArrayList<Integer>();
        ArrayList<Integer> prime = new ArrayList<Integer>();
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter some numbers:");
        while (scan.hasNextInt()) {

            numbers.add(scan.nextInt());

        }
        //System.out.println("My List:" + numbers);

        Iterator<Integer> myListIterator = numbers.iterator();

        while (myListIterator.hasNext()) {

            Integer num = myListIterator.next();

            if (checkPalindrome(num)) {
                palindrome.add(num);
              
            }

            if (checkPrime(num)) {
                prime.add(num);

            }
        }
        System.out.println("Largest number:" + getLargest(numbers));
        System.out.println("Smallest number:" + getSmallest(numbers));
        System.out.println("Palindromes:" + palindrome);
        System.out.println("Prime numbers:" + prime);

    }

    //method for getting  the largest number
    public static int getLargest(ArrayList<Integer> inputList) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < inputList.size(); i++) {
            if (inputList.get(i) > max) {
                max = inputList.get(i);
            }
        }
        return max;
    }

    //method for getting  the smallest number
    public static int getSmallest(ArrayList<Integer> inputList) {
        int min = Integer.MAX_VALUE;
        for (int i = 1; i < inputList.size(); i++) {
            if (inputList.get(i) < min) {
                min = inputList.get(i);
            }
        }
        return min;
    }

    //method for check if a number is prime
    public static boolean checkPrime(int n) {
        for (int i = 2; i <= n / 2; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    //method for check if a number is palindrome
    public static boolean checkPalindrome(int n) {
        int palindrome = n;
        int ogl = 0;

        while (n > 0) {
            int code = n % 10;
            ogl = (ogl * 10) + code;
            n = n / 10;
        }
        if (palindrome == ogl) {
            return true;
        }
        return false;
    }

}
